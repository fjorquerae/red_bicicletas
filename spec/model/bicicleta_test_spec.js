var Bicicleta = require('../../models/bicicleta')

/* para correr el test usar el comando "npm test" */

beforeEach(() => { Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});

describe('Bicicleta.allBicis', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.17908359129338, -70.76991150528553]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });

});

describe('Bicicleta.findById', () => {
    it('debe devolver al bici de id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "rojo", "urbana");
        var aBici2 = new Bicicleta(2, "verde", "montaña");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);


        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);



    });
});