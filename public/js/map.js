  var map = L.map('main_map').setView([-34.165783359073835, -70.76545954709461], 18);
  // -34.165783359073835, -70.76545954709461
  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data © OpenStreetMap contributors, CC-BY-SA, Imagery © CloudMade',
      maxZoom: 20
  }).addTo(map);

  /* L.marker([-34.16841095117525, -70.7438496013209]).addTo(map);
  L.marker([-34.171677600107984, -70.76170238445391]).addTo(map);
  L.marker([-34.164718065362266, -70.75208934738228]).addTo(map); */

  $.ajax({
      dataType: "JSON",
      url: "/api/bicicletas",
      success: function(result) {
          console.log(result);
          result.bicicletas.forEach(function(bici) {
              L.marker([bici.ubicacion], { title: bici.id }).addTo(map);
          });
      }
  })