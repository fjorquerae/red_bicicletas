const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    })
}



module.exports = mongoose.model('Bicicleta', bicicletaSchema);

bicicletaSchema.method.toString = function() {
    return 'code: ' + this.code + '| color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb)
};


var Bicicleta = function(id, color, modelo, ubicacion) {
        this.id = id
        this.color = color
        this.modelo = modelo
        this.ubicacion = ubicacion
    }
    //modificada por la funcion mongoose de mas arriba
    /*Bicicleta.prototype.toString = function() {
        return 'id: ' + this.id + ' | color: ' + this.color
    } */

Bicicleta.allBicis = []
Bicicleta.add = function(aBici) {
    const index = Bicicleta.findIndexById(aBici.id)
    if (index == null) {
        Bicicleta.allBicis.push(aBici)
    } else {
        console.error(`Ya existe una bicicleta con Id = ${aBici.id}`)
    }
}

Bicicleta.findIndexById = function(aBiciId) {
    const condicion = (e) => e.id == aBiciId;
    const index = Bicicleta.allBicis.findIndex(condicion)
    return index < 0 ? null : index
}

Bicicleta.findById = function(aBiciId) {
    const index = Bicicleta.findIndexById(aBiciId)
    return index == null ? null : Bicicleta.allBicis[index]
}

Bicicleta.removeById = function(aBiciId) {
    const index = Bicicleta.findIndexById(aBiciId)
    if (index != null) { Bicicleta.allBicis.splice(index, 1) }
}


var a = new Bicicleta(1, 'rojo', 'urbana', [-34.17908359129338, -70.76991150528553])
var b = new Bicicleta(2, 'verde', 'montaña', [-34.18105763299052, -70.7759061625809])

Bicicleta.add(a)
Bicicleta.add(b)


module.exports = Bicicleta